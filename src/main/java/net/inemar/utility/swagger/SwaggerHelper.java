package net.inemar.utility.swagger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collection;

import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.StaticHttpHandlerBase;
import org.glassfish.grizzly.http.util.HttpStatus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class SwaggerHelper extends StaticHttpHandlerBase {
	
	public static void deliver(HttpServer server) throws IOException {
		deliver(server,"/doc",SwaggerHelper.class);
	}

	public static void deliver(HttpServer server,@SuppressWarnings("rawtypes") Class docRoot) throws IOException {
		deliver(server,"/doc",docRoot);
	}
	
	
	public static void deliver(HttpServer server,String url,@SuppressWarnings("rawtypes") Class docRoot) throws IOException {
		if (!url.startsWith("/")) {
			url="/"+url;			
		};
		if (url.endsWith("/")) {
			url=url.substring(0,url.length()-1);
		};
		Collection<NetworkListener> col=server.getListeners();
		if (col.isEmpty()) {
			throw new IOException("No network listener is present");
		};
		URL murl=null;
		for(NetworkListener act:col) {
			String host=act.getHost();
			int port=act.getPort();
			murl=new URL("http",host,port,url);
			break;
		}

		
		server.getServerConfiguration().addHttpHandler(new SwaggerHelper(murl,docRoot,url),url);
	}

	
	URL baseUrl;
	@SuppressWarnings("rawtypes")
	Class docRoot;
	String hostname=null;
	String httpdoc=null;;
	String path;
	
	
	String absolutePath;
	public SwaggerHelper(URL baseUrl,@SuppressWarnings("rawtypes") Class docRoot) {
		this(baseUrl,docRoot,"");
	}

	public SwaggerHelper(URL baseUrl,@SuppressWarnings("rawtypes") Class docRoot,String path) {
		this.baseUrl=baseUrl;
		this.docRoot=docRoot;
		this.path=path;
		absolutePath="/"+docRoot.getPackage().getName().replace(".", "/")+path+"/";
		//System.out.println("GET "+abspath);
		this.handler=new MyCLStaticHttpHandler(docRoot.getClassLoader(),absolutePath);			
		hostname=baseUrl.getProtocol()+"://"+baseUrl.getHost()+":"+baseUrl.getPort();
		/*
		if (httpdoc.startsWith("/")) {
			;
		} else {
			httpdoc="/"+httpdoc;
		}
		this.httpdoc=httpdoc;*/
	}
	
	
	private MyCLStaticHttpHandler handler=null;
	
	
	private static class MyCLStaticHttpHandler extends CLStaticHttpHandler {

		public MyCLStaticHttpHandler(ClassLoader arg0, String...arg1) {
			super(arg0, arg1);
			this.setFileCacheEnabled(false);
			// TODO Auto-generated constructor stub
		}
		
		
		protected boolean handle(String url, Request arg1, Response response)  throws Exception {
			return super.handle(url, arg1, response);
		}
		
	}
	

	@Override
	protected boolean handle(String url, Request arg1, Response response)
			throws Exception {
		//System.out.println("X:"+url+":X");

		if (url.equals("")) {
			//send back redirect to index.html
			String redirect=""+baseUrl+"/index.html";
			response.sendRedirect(redirect);		
			return true;
		};
		if (url.equals("/index.html")) {
			if (handler.handle(url, arg1, response)) {
				return true;
			}
			String redirect=""+baseUrl+"/apidocs/index.html";
			response.sendRedirect(redirect);		
			return true;
			/*
			try {
				InputStream in=docRoot.getResourceAsStream("."+path+"/index.html");
				if (in==null) {
					String redirect=""+baseUrl+"/apidocs/index.html";
					response.sendRedirect(redirect);		
					return true;					
				}
				in.close();
				
				return handler.handle(url, arg1, response);
				
			} catch (IOException ex) {
				ex.printStackTrace();
			}*/
		}
		if (url.equals("/apidocs")) {
			String redirect=""+baseUrl+"/apidocs/index.html";
			response.sendRedirect(redirect);		
			return true;	
		}
		if (url.equals("/apidocs/api")) {
			return deliverSwaggerFile(true,"service.json",response);
		}
		if (url.equals("/apidocs/index.html/service.json")) {
			return deliverSwaggerFile(true,"service.json",response);
		}
		if (url.equals("/apidocs/service.json")) {
			return deliverSwaggerFile(true,"service.json",response);
		}
		if (url.startsWith("/apidocs") & url.endsWith(".json")) {
			int idx=url.lastIndexOf('/');				
			return deliverSwaggerFile(false,url.substring(idx+1),response);
		}
		return handler.handle(url, arg1, response);
	}
	
	boolean deliverSwaggerFile(boolean base,String url, Response response) {
		Gson gson=(new GsonBuilder()).setPrettyPrinting().create();
		JsonParser parser=new JsonParser();
		//System.out.println("FF "+"."+path+"/apidocs/"+url);
		//URL myurl=docRoot.getResource(absolutePath+"apidocs/"+url);
		//System.out.println(absolutePath+"apidocs/"+url);
		//System.out.println(url+"   "+myurl);
		if (url.startsWith("/")) {
			url=url.substring(1);
		}
		JsonElement root=parser.parse(new InputStreamReader(docRoot.getResourceAsStream(absolutePath+"apidocs/"+url)));
		if (base) {
			root.getAsJsonObject().addProperty("basePath",baseUrl.toString()+"/apidocs");
		} else {
			root.getAsJsonObject().addProperty("basePath",hostname);
		}
		String str=gson.toJson(root);
		//System.out.println("Response "+str);
		response.setStatus(HttpStatus.OK_200);
		response.setContentType("application/json");
		response.setContentLength(str.length());
    	response.addHeader("Access-Control-Allow-Origin", "*");
    	response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");            
    	response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia");

		try {
			response.getWriter().write(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

}
