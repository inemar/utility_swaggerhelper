# README #


### What is this repository for? ###

* Swagger is an awesome tool to document Rest Interface. This little helper class provides the swagger docu online
* More info about swagger you find at [Swagger](http://swagger.io/)
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* First Write a Java Rest Interface with Jersey and Grizzly as deployer


    	@Path("/rest")
    	@Api(value="/rest",description = "Demo of Swagger Api")
    	public class RestInterface {
    	
    		@GET
    		@Path("/hello")
    		@Produces(MediaType.APPLICATION_JSON)
    		@ApiOperation(value="Do a hello World")
    		public HelloWorld getHello() {
    		
    			HelloWorld hello=new HelloWorld();
    			hello.hello="Hello world from Swagger";
    			hello.date=""+new Date();
    			return hello;
    		
    		}
    
    	
    		public static class HelloWorld {
    			public String hello;
    			public String date;
    		}
    	}
    
* Next Step generate using Maven the Swagger docu

    
    	<build>
    		<plugins>
    			<plugin>
    				<groupId>org.apache.maven.plugins</groupId>
    				<artifactId>maven-javadoc-plugin</artifactId>
    				<version>2.9.1</version>
    				<executions>
    					<execution>
    						<id>generate-service-docs</id>
    						<phase>generate-resources</phase>
    						<configuration>
    							<doclet>com.carma.swagger.doclet.ServiceDoclet</doclet>
    							<docletArtifact>
    								<groupId>com.carma</groupId>
    								<artifactId>swagger-doclet</artifactId>
    								<version>1.0.4.2</version>
    							</docletArtifact>
    							<reportOutputDirectory>${basedir}/src/main/resources/net/inemar/utility/swagger/doc</reportOutputDirectory>
    							<useStandardDocletOptions>false</useStandardDocletOptions>
    							<additionalparam>-apiVersion 1 -docBasePath http://0.0.0.0:0
    								-apiBasePath http://0.0.0.0:0</additionalparam>
    						</configuration>
    						<goals>
    							<goal>javadoc</goal>
    						</goals>
    					</execution>
    				</executions>
    			</plugin>
    		</plugins>
    		<pluginManagement>
    			<plugins>
    				<!--This plugin's configuration is used to store Eclipse m2e settings 
    					only. It has no influence on the Maven build itself. -->
    				<plugin>
    					<groupId>org.eclipse.m2e</groupId>
    					<artifactId>lifecycle-mapping</artifactId>
    					<version>1.0.0</version>
    					<configuration>
    						<lifecycleMappingMetadata>
    							<pluginExecutions>
    								<pluginExecution>
    									<pluginExecutionFilter>
    										<groupId>
    											org.apache.maven.plugins
    										</groupId>
    										<artifactId>
    											maven-javadoc-plugin
    										</artifactId>
    										<versionRange>
    											[2.9.1,)
    										</versionRange>
    										<goals>
    											<goal>javadoc</goal>
    										</goals>
    									</pluginExecutionFilter>
    									<action>
    										<execute></execute>
    									</action>
    								</pluginExecution>
    							</pluginExecutions>
    						</lifecycleMappingMetadata>
    					</configuration>
    				</plugin>
    			</plugins>
    		</pluginManagement>
    	</build>
    
* This produces the docu in the output folder 

		${basedir}/src/main/resources/net/inemar/utility/swagger/doc

* To deliver the source you add the following dependency

    		<dependency>
    			<groupId>net.inemar.utility</groupId>
    			<artifactId>SwaggerHelper</artifactId>
    			<version>0.0.1</version>
    		</dependency>

* Create a dummy class in the package net.inemar.utility.swagger.local Called DocRoot
* And add a simple http handler that uses the folder /net/inemar/utility/swagger/doc

		SwaggerHelper.deliver(server);
 
 	If you use a different location or URL use the fully configurable form

	    server.getServerConfiguration().addHttpHandler(new SwaggerHelper(new URL(baseUri.toURL(),"/doc"),DocRoot.class),"/doc");
    
* Go with your browser to /doc and you will see the documentation

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact